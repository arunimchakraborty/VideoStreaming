import {
	AppShell,
	useMantineTheme,
	MantineProvider,
} from "@mantine/core";
import { Routes, Route, useNavigate } from "react-router-dom";
import VideoList from "./Pages/VideoList";
import VideoPage from "./Pages/VideoPage";
import { Outlet } from "react-router-dom";
import UploadVideo from "./Pages/UploadVideo";
import HeaderSec from "./components/Header";

export default function App() {
	const theme = useMantineTheme();
	const navigate = useNavigate();
	return (
		<MantineProvider withGlobalStyles withNormalizeCSS>
			<AppShell
				styles={{
					main: {
						background:
							theme.colorScheme === "dark"
								? theme.colors.dark[8]
								: theme.colors.gray[0],
					},
				}}
				navbarOffsetBreakpoint="sm"
				asideOffsetBreakpoint="sm"
				header={
					<HeaderSec navigate={navigate} />
				}
			>
				<Routes>
					<Route path="/" element={<VideoList />} />
					<Route path="/upload" element={<UploadVideo />} />
					<Route path="/video/:id" element={<VideoPage />} />
				</Routes>
			</AppShell>
			<Outlet />
		</MantineProvider>
	);
}
