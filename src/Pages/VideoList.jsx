import React, { useEffect, useState } from "react";
import VideoCard from "../features/videoList/components/VideoCard"
import { Grid } from "@mantine/core";
import config from "../utils/config";
import useVideoList from "../features/videoList/hooks/useVideoList";

export default function VideoList() {

	const [videos] = useVideoList()

	return (
		<div>
			{Array.isArray(videos) && videos ? (
				<Grid>
					{videos.map((e) => {
						console.log(`${config.url}${e.Key}`);
						return (
							<Grid.Col key={e.ETag} span={4}>
								<VideoCard name={e.Key} url={`${config.url}${e.Key}`} />
							</Grid.Col>
						);
					})}
				</Grid>
			) : null}
		</div>
	);
}
