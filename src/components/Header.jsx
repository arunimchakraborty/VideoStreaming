import { Header, Text, UnstyledButton } from "@mantine/core";
import React from "react";

export default function HeaderSec({navigate}) {
	return (
		<Header height={{ base: 20, md: 50 }} p="md">
			<div style={{ display: "flex", alignItems: "center", height: "100%" }}>
				<UnstyledButton onClick={() => navigate("/")}>
					<Text>Watching</Text>
				</UnstyledButton>
				<UnstyledButton onClick={() => navigate("/upload")}>
					<Text style={{ paddingLeft: 60 }}>Upload</Text>
				</UnstyledButton>
			</div>
		</Header>
	);
}
