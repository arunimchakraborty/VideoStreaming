module.exports = {
  bucketName: "video.arunim.com",
  url: "https://s3.amazonaws.com/video.arunim.com/",
  access: process.env.REACT_APP_ACCESS || undefined,
  secret: process.env.REACT_APP_SECRET || undefined
}
