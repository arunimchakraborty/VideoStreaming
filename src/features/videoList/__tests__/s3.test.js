const AWS = require('aws-sdk');
const s3 = require('../services/s3');
const config = require('../../../utils/config');

describe('s3', () => {
  it('should be an instance of AWS.S3', () => {
    expect(s3).toBeInstanceOf(AWS.S3);
  });

  it('should have the correct access key ID and secret access key in the AWS.config.credentials property', () => {
    const { accessKeyId, secretAccessKey } = AWS.config.credentials;
    expect(accessKeyId).toBe(config.access);
    expect(secretAccessKey).toBe(config.secret);
  });

  it('should have the correct API version', () => {
    expect(s3.config.apiVersion).toBe('2006-03-01');
  });
});
