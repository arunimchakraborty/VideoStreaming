const config = require('../../../utils/config')
const s3 = require('../services/s3')
const listVideo = require('../services/listVideo')



jest.mock('../services/s3.js', () => ({
  listObjects: jest.fn().mockReturnValue({
    Contents: [{ key: 'video1.mp4' }, { key: 'video2.mp4' }],
  }),
}));

describe('listVideo', () => {
  it('should return an array of non-zero length', async () => {
    const result = await listVideo();
    expect(Array.isArray(result)).toBe(true);
    expect(result.length).toBeGreaterThan(0);
  });

  it('should handle errors gracefully', async () => {
    const mockError = new Error('Something went wrong');
    jest.spyOn(console, 'log').mockImplementation(() => {});
    jest.spyOn(window, 'alert').mockImplementation(() => {});
    jest.spyOn(s3, 'listObjectsV2').mockRejectedValueOnce(mockError);

    const result = await listVideo();
    expect(result).toEqual([]);
    expect(s3.listObjectsV2).toHaveBeenCalledWith({ Bucket: config.bucketName });
    expect(console.log).toHaveBeenCalledWith(mockError.stack);
    expect(window.alert).toHaveBeenCalledWith(mockError.message);
  });
});
