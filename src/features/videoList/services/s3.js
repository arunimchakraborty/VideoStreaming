const AWS = require('aws-sdk');
const config = require('../../../utils/config');

AWS.config.update({
	accessKeyId: config.access,
	secretAccessKey: config.secret,
});

// Create an S3 service object
const s3 = new AWS.S3({ apiVersion: "2006-03-01" });
module.exports = s3