import { useEffect, useState } from "react";
import s3 from "../services/s3";
import config from "../../../utils/config";

export default function useVideoList() {
	const [videos, setVideos] = useState([]);

	useEffect(() => {
		s3.listObjects({ Bucket: config.bucketName }, (err, data) => {
      if (err) return alert(err);
      else {
        console.log("Data Returned - ", data.Contents);
        setVideos(data.Contents)
      }
    })
	}, []);

	return [videos]
}
