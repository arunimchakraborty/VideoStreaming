import VideoCard from "./components/VideoCard";
const useListVideos = require('../videoList/hooks/useListVideos')

export { useListVideos, VideoCard };
