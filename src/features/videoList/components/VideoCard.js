import { useNavigate } from "react-router-dom";
import useStyles from "../styles/mainStyle";
import { Button, Card, Group, Text } from "@mantine/core";

export default function VideoCard(props) {
	const navigate = useNavigate();
	const { classes } = useStyles();

	return (
		<Card withBorder radius="md" className={classes.card}>
			<Group position="center" mt="md">
				<div>
					<Text weight={500}>{props.name}</Text>
				</div>
			</Group>

			<Card.Section className={classes.section}>
				<Group spacing={30}>
					<Button
						radius="xl"
						style={{ flex: 1 }}
						onClick={() => {
							navigate(`video/${props.name}`);
						}}
					>
						Watch now
					</Button>
				</Group>
			</Card.Section>
		</Card>
	);
}